﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using AForge;
using AForge.Imaging.Filters;
using AForge.Imaging;
using AForge.Video;
using AForge.Video.DirectShow;
using Point = System.Drawing.Point;


namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private FilterInfoCollection VideoCapTureDevices;
        private VideoCaptureDevice Finalvideo;
                  

        public Form1()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
           
        }

   
        
        private void Form1_Load(object sender, EventArgs e)
        {
            VideoCapTureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo VideoCaptureDevice in VideoCapTureDevices)
            {

                comboBox1.Items.Add(VideoCaptureDevice.Name);

            }

            comboBox1.SelectedIndex = 0;

        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            Finalvideo = new VideoCaptureDevice(VideoCapTureDevices[comboBox1.SelectedIndex].MonikerString);
            Finalvideo.NewFrame += new NewFrameEventHandler(Finalvideo_NewFrame);
            Finalvideo.DesiredFrameRate = 20;
            Finalvideo.DesiredFrameSize = new Size(320, 240);
            Finalvideo.Start();
        }

        void Finalvideo_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {

            Bitmap image = (Bitmap)eventArgs.Frame.Clone(); //Aynalama
            Bitmap image1 = (Bitmap)eventArgs.Frame.Clone();//EUCLİDEAN  
            Bitmap image2 = (Bitmap)eventArgs.Frame.Clone();//EUCLİDEAN 
            Bitmap image3 = (Bitmap)eventArgs.Frame.Clone();//EUCLİDEAN

            Mirror filter1 = new Mirror(false, true);
            filter1.ApplyInPlace(image);
            pictureBox1.Image = image;

           
                EuclideanColorFiltering filter = new EuclideanColorFiltering();
                filter.CenterColor = new RGB(Color.FromArgb(215, 0, 0)); 
                filter.Radius = 100;
                filter.ApplyInPlace(image1);
    
                nesnebul(image1);


            
                EuclideanColorFiltering filter2 = new EuclideanColorFiltering();
                filter.CenterColor = new RGB(Color.FromArgb(30, 144, 255));
                filter.Radius = 100;
                filter.ApplyInPlace(image2);
                
                nesnebul2(image2);
                
            
           
            
                EuclideanColorFiltering filter3 = new EuclideanColorFiltering();
                filter.CenterColor = new RGB(Color.FromArgb(0, 200, 0));
            filter.Radius = 100;
                filter.ApplyInPlace(image3);

                nesnebul3(image3);
            
            
        }

        public void nesnebul(Bitmap image)
        { 
             //nesneyi tespit etmesi için 
            BlobCounter blobCounter = new BlobCounter();
            blobCounter.MinWidth = 5;
            blobCounter.MinHeight = 5;
            blobCounter.FilterBlobs = true;
            blobCounter.ObjectsOrder = ObjectsOrder.Size;
           
             Mirror filter2 = new Mirror(false, true);
            filter2.ApplyInPlace(image);
          

            blobCounter.ProcessImage(image);
            Rectangle[] rects = blobCounter.GetObjectsRectangles();
       
          
            pictureBox2.Image = image;


            //nesnenin boyutu kadar dikdörtgene algılasın
            
            for (int i = 0; rects.Length > i; i++)
                {  
                  
                    Rectangle objectRect = rects[i];
                    Graphics g = pictureBox1.CreateGraphics();
                    using (Pen pen = new Pen(Color.FromArgb(252, 3, 26), 2))
                    {
                        string k = "Kırmızı";
                        g.DrawRectangle(pen, objectRect);
                        g.DrawString(k, new Font("Arial", 12), Brushes.Red, objectRect);
                    }
                   
                    g.Dispose(); 

                   
                }
            }
        public void nesnebul2(Bitmap image)
        {
            
            BlobCounter blobCounter = new BlobCounter();
            blobCounter.MinWidth = 5;
            blobCounter.MinHeight = 5;
            blobCounter.FilterBlobs = true;
            blobCounter.ObjectsOrder = ObjectsOrder.Size;

            Mirror filter2 = new Mirror(false, true);
            filter2.ApplyInPlace(image);


            blobCounter.ProcessImage(image);
            Rectangle[] rects = blobCounter.GetObjectsRectangles(); 
            

            pictureBox2.Image = image;

            
            for (int i = 0; rects.Length > i; i++)
            {

                Rectangle objectRect = rects[i];
                Graphics g = pictureBox1.CreateGraphics();
                using (Pen pen = new Pen(Color.FromArgb(30, 144, 255), 2))
                { string m = "Mavi";
                    g.DrawRectangle(pen, objectRect);
                    g.DrawString(m, new Font("Arial", 12), Brushes.Blue, objectRect);
                }
                g.Dispose();


            }
        }
        public void nesnebul3(Bitmap image)
        {
            
            BlobCounter blobCounter = new BlobCounter();
            blobCounter.MinWidth = 5;
            blobCounter.MinHeight = 5;
            blobCounter.FilterBlobs = true;
            blobCounter.ObjectsOrder = ObjectsOrder.Size;

            Mirror filter2 = new Mirror(false, true);
            filter2.ApplyInPlace(image);


            blobCounter.ProcessImage(image);
            Rectangle[] rects = blobCounter.GetObjectsRectangles();
            

            pictureBox2.Image = image;



            
            for (int i = 0; rects.Length > i; i++)
            {

                Rectangle objectRect = rects[i];
                Graphics g = pictureBox1.CreateGraphics();
                using (Pen pen = new Pen(Color.FromArgb(0, 200, 0), 2))
                {
                    string y = "Yeşil";
                    g.DrawRectangle(pen, objectRect);
                    g.DrawString(y, new Font("Arial", 12), Brushes.Green, objectRect);
                }

                g.Dispose();


            }
        }

       

        private void button2_Click(object sender, EventArgs e)
        {

            if (Finalvideo.IsRunning)
            {
                Finalvideo.Stop();                
            }
        }
       
      

        private void button3_Click(object sender, EventArgs e)
        {

            if (Finalvideo.IsRunning)
            {
                Finalvideo.Stop();

            }

            Application.Exit();
        }

     
    }


}


